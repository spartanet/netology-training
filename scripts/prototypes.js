﻿// Задача 1: Конструктор Student

function Student(name, point) {
	this.name = name;
	this.point = point != undefined ? point : 0;
}

Student.prototype.show = function () {
	console.log('Студент %s набрал %d баллов', this.name, this.point);
};

// Задача 2: Создание объекта StudentList

var isEven = function (index) {
	return !(index % 2);
}

var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];

function StudentList(groupName, pointsOfStudents) {
	this.groupName = groupName;
	this.importStudentsNPoints(pointsOfStudents);
}

StudentList.prototype = Object.create(Array.prototype);
StudentList.prototype.constructor = StudentList;

StudentList.prototype.add = function (studentName, point) {
	this.push(new Student(studentName, point));
}

StudentList.prototype.parseStudent = function (arrayItem, index, studentsAndPointsArray) {
	if (isEven(index)) {
		this.push(new Student(arrayItem, studentsAndPointsArray[index + 1]));
	};
}

StudentList.prototype.importStudentsNPoints = function (pointsOfStudents) {
	if (pointsOfStudents !== undefined && pointsOfStudents instanceof Array) {
		pointsOfStudents.forEach(this.parseStudent.bind(this));
	}
}

// Задача 3: Создание списка студентов на основе массива studentsAndPoints

var hj2 = new StudentList("HJ-2", studentsAndPoints);

// Задача 4: Добавление дополнительных студентов

hj2.add("Капитан Флинт", 80);
hj2.add("Джон Сильвер", 100);
hj2.add("Слепой Пью", 50);

// Задача 5: Создание пустого списка студентов и его заполнение

var html7 = new StudentList("HTML-7");

html7.importStudentsNPoints(["Джим Хокинс", 10, "Сквайр Трелони", 90, "Доктор Ливси", 110, "Капитан Смоллетт", 70]);

// Задача 6: Реализация метода show

StudentList.prototype.show = function () {
	console.log("Группа %s (%d студентов):", this.groupName, this.length);
	this.forEach(function (student) {
		student.show();
	});
}

hj2.show();
html7.show();

// Задача 7: Перевод студента между группами

console.log("====================================================================");

html7.push(hj2.pop());

hj2.show();
html7.show();

// Дополнительное задание: поиск студента с максимальным баллом

// Для класса Student переопределяем базовую реализацию преобразования к численному примитиву
Student.prototype.valueOf = function () {
	return this.point;
}

StudentList.prototype.max = function () {
	// Функция Math.max будет работать на массиве студентов, потому что для Student переопределен valueOf
	var maxPoints = Math.max.apply(null, this);
	// Студентов с наибольшим баллом может быть несколько. Получим их всех с помощью наследуемого от Array метода
	return this.filter(function (student) {
		return student == maxPoints; // Здесь опять используем приведение объекта к примитиву путем нестрогого сравнения объекта с числом
	});
}

