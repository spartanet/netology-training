﻿// Задача 1

function multiply(a, b) {
	return (b != undefined) ? a * b : a * a;
}

var a = 5, b = 6;
console.log("Вызов функции multiply с одним аргументом. Результат квадрата числа %d = %d", a, multiply(a));
console.log("Вызов функции multiply с двумя аргументами. Результат перемножения чисел %d х %d = %d", a, b, multiply(a, b));

// Задача 2

console.log("====================================================================");
function getCounter() {
	var i = 0;

	return function (offset) {
		return i += offset;
	};
}

var counter = getCounter();
var v = 5;
console.log("Вызываем счетчик с параметром %d: ", v, counter(v));
v = 6;
console.log("Вызываем счетчик с параметром %d: ", v, counter(v));

// Задача 3

console.log("====================================================================");
console.log("Собираем рассказ из слов");
function getArticleMaker() {
	var article = null;

	return function chtoNapisanoPerom(word) {
		return article === null ? article = word : article += " " + word;
	};
}

var articleMaker = getArticleMaker();
console.log(articleMaker('Буря'));
console.log(articleMaker('мглою'));
console.log(articleMaker('небо'));
console.log(articleMaker('кроет'));
console.log(articleMaker('вихри'));
console.log(articleMaker('снежные'));
console.log(articleMaker('крутя'));
