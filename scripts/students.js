var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];

console.log('====================================');
console.log('Список студентов:');

var leader = '';		// Имя студента с максимальными баллами
var leaderPoints = 0;	// Его баллы

for (var i = 0; i < studentsAndPoints.length; i += 2) {
	var currentPoints = studentsAndPoints[i + 1];

	console.log('Студент %s набрал %d баллов', studentsAndPoints[i], currentPoints);

	if (currentPoints > leaderPoints) {
		leader = studentsAndPoints[i];
		leaderPoints = currentPoints;
	}
}

console.log('====================================');
console.log('Студент набравший максимальный балл:');
console.log('Студент %s имеет максимальный бал %d', leader, leaderPoints);

studentsAndPoints.push('Николай Фролов', 0, 'Олег Боровой', 0);

console.log('====================================');
console.log('Добавлены новые студенты:');
console.log(studentsAndPoints.toString());

var studentsToUpdate = ['Антон Павлович', 10, 'Николай Фролов', 10]; // Массив обновлений рейтинга студентов


for (var i = 0; i < studentsToUpdate.length; i += 2) {
	var pos = studentsAndPoints.indexOf(studentsToUpdate[i]);
	if (pos > 0) {
		studentsAndPoints[pos + 1] += studentsToUpdate[i + 1];
	}
}

console.log('====================================');
console.log('Обновлены баллы студентов:');
console.log(studentsAndPoints.toString());

console.log('====================================');
console.log('Студенты не набравшие баллов:');
for (var i = 0; i < studentsAndPoints.length; i += 2) {
	if (studentsAndPoints[i + 1] === 0) {
		console.log(studentsAndPoints[i]);
	}
}

console.log('====================================');
console.log('Удалены студенты не набравшие баллов:');


for (var i = studentsAndPoints.length - 2; i >= 0; i -= 2) {
	if (studentsAndPoints[i + 1] === 0) {
		studentsAndPoints.splice(i, 2);
	}
}
console.log(studentsAndPoints.toString());


