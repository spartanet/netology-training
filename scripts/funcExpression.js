﻿var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];

// Задача 1: Формирование исходных массивов без использования циклов

var points = studentsAndPoints.filter(function (val, i, arr) { return i % 2; });
var students = studentsAndPoints.filter(function (val, i, arr) { return !(i % 2); });

console.log("Студенты:", students.toString());
console.log("Баллы:", points.toString());

// Задача 2: Вывод списка студентов без использования циклов
console.log('====================================');
console.log('Список студентов:');

students.forEach(function (val, i, arr) {
	console.log('Студент %s набрал %d баллов', val, points[i]);
});

// Задача 3: Поиск студента, набравшего наибольшее количество баллов
console.log('====================================');

var iLeader;
var leaderPoints = -1;

points.forEach(function (val, i, arr) {
	if (val > leaderPoints) {
		leaderPoints = val;
		iLeader = i;
	}
});
console.log('Студент набравший максимальный балл: %s (%d)', students[iLeader], leaderPoints);

// Задача 4: Добавление баллов отдельным студентам
console.log('====================================');

var studentsToUpdate = ['Ирина Овчинникова', 'Александр Малов']; // Массив имен студентов для обновления

var points = points.map(function (val, i, arr) {
	return (studentsToUpdate.indexOf(students[i]) >= 0) ? val += 30 : val;
	}
);

console.log('Студентам %s добавлено по 30 баллов', studentsToUpdate.toString());

students.forEach(function (val, i, arr) {
	console.log('Студент %s набрал %d баллов', val, points[i]);
});

// Задача 5: Реализация функции getTop
console.log('====================================');

function getTop(topNum) {
	if (topNum == undefined || topNum < 1)
		topNum = 3; // Зададим значение по умолчанию

	console.log('Топ-%d:', topNum);

	// Синхронно сортируем оба массива алгоритмом вставки
	for (var j = 1; j < points.length; j++) {
		var point = points[j];
		var name = students[j];
		var i = j - 1;
		while (i >= 0 && points[i] > point) {
			points[i + 1] = points[i];
			students[i + 1] = students[i];
			i--;
		}
		points[i + 1] = point;
		students[i + 1] = name;
	}

	// Формируем результирующий массив
	var resArr = [];
	for (i = 0, j = students.length-1; i < topNum; i++, j--) {
		resArr.push(students[j]);
		resArr.push(points[j]);
	}

	return resArr;
}

var print = function (val, i, arr) {
	if (!(i % 2))
		console.log('%s - %d баллов', val, arr[i + 1]);
};

getTop().forEach(print);
getTop(5).forEach(print);
