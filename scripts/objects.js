﻿var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];

// Задача 1: Создание массива объектов

var students = [];
var print = function () { console.log('Студент %s набрал %d баллов', this.name, this.point); };

function createStudent(name, point) {
	return {
		name: name,
		point: point != undefined ? point : 0,
		show: print
	};
}

studentsAndPoints.forEach(function (val, i, arr) {
	if (!(i % 2))
		students.push(createStudent(val, arr[i + 1]));
});

// Задача 2: Добавление новых студентов

students.push(createStudent('Николай Фролов'));
students.push(createStudent('Олег Боровой'));

// Дополнительная задача: поиск по имени

students.findByName = function (name) {
	return this.find(function (val) {
		return val.name === name;
	});
}

// Задача 3: Добавление баллов студентам

students.setPoints = function (name, points) {
	var founded = this.findByName(name);

	if (founded != undefined)
		founded.point += points;
}

students.setPoints('Ирина Овчинникова', 30);
students.setPoints('Александр Малов', 30);
students.setPoints('Николай Фролов', 10);

// Задача 4: Вывод списка студентов с баллом 30 и выше

console.log('Список студентов:');
students.forEach(function (val) {
	if (val.point >= 30)
		val.show();
});

// Задача 5: Вычисление количества выполненных работ

students.forEach(function (val) {
	val.worksAmount = val.point / 10;
});
