﻿// Определение класса, формирующего вид формы аутентификации

function AuthViewBuilder() {
	this.HIDE_CLASS = 'hide';

	this.authForm	= document.querySelector('.authForm');
	this.profileInfo= document.querySelector('div[data-role=json-request-user-profile]');
	this.preloader	= document.querySelector('.preloader');

	this.name		= document.querySelector('span[data-role=json-request-user-profile-fullname]');
	this.jobProfile = document.querySelector('.jobProfile');
	this.country	= document.querySelector('span[data-role=json-request-user-profile-country]');
	this.hobbies	= document.querySelector('span[data-role=json-request-user-profile-hobbies]');
	this.login		= document.querySelector('input[type=email]');
	this.password	= document.querySelector('input[type=password]');
	this.errMsg		= document.querySelector('div[data-role=json-request-error]');

	this.signUpBtn = document.querySelector('button[data-role=json-request-form-submit]');
	this.signUpBtn.builder = this;
	this.signUpBtn.onclick = function (e) {
		e.preventDefault();
		this.builder.authHelper.auth(this.builder.login.value, this.builder.password.value);
	}

	this.signOffBtn = document.querySelector('button[data-role=json-request-user-profile-logout]');
	this.signOffBtn.builder = this;
	this.signOffBtn.onclick = function (e) {
		e.preventDefault();
		this.builder	.signOff();
	}

	this.authHelper = new AuthHelper();
	this.authHelper.builder = this;
	this.authHelper.onauthbegin = this.processAuthBegin;
	this.authHelper.onauthsucceed = this.processAuthSucceed;
	this.authHelper.onauthfailed = this.processAuthFailed;
}

AuthViewBuilder.prototype.show = function show(element) {
	element.classList.remove(this.HIDE_CLASS);
}

AuthViewBuilder.prototype.hide = function hide(element) {
	element.classList.add(this.HIDE_CLASS);
}

AuthViewBuilder.prototype.fillProfileInfo = function fillProfileInfo(profileData) {
	// Предполагаем, что все поля профайла, кроме списка хобби - обязательные к заполнению, и поэтому указаны всегда
	this.name.textContent = profileData.name + ' ' + profileData.lastname;
	this.country.textContent = profileData.country;
	this.jobProfile.textContent = profileData.job.company + ', ' + profileData.job.position;

	if (profileData.hobbies instanceof Array) {
		this.hobbies.textContent = profileData.hobbies.join(', ');
	}
}

AuthViewBuilder.prototype.clearProfileInfo = function clearProfileInfo() {
	this.name.textContent = '';
	this.jobProfile.textContent = '';
	this.country.textContent = '';
	this.hobbies.textContent = '';
}

AuthViewBuilder.prototype.clearAuthForm = function clearAuthForm() {
	this.login.value = '';
	this.password.value = '';
	this.errMsg.textContent = '';
}

AuthViewBuilder.prototype.processAuthBegin = function () {
	this.builder.hide(this.builder.authForm);
	this.builder.show(this.builder.preloader);
}

AuthViewBuilder.prototype.processAuthSucceed = function (profileData) {
	this.builder.hide(this.builder.preloader);
	this.builder.hide(this.builder.errMsg);
	this.builder.hide(this.builder.authForm);
	this.builder.clearAuthForm();

	this.builder.fillProfileInfo(profileData);
	this.builder.show(this.builder.profileInfo);
}

AuthViewBuilder.prototype.processAuthFailed = function (message) {
	this.builder.hide(this.builder.preloader);
	this.builder.errMsg.textContent = message;
	this.builder.show(this.builder.authForm);
	this.builder.show(this.builder.errMsg);
}

AuthViewBuilder.prototype.signOff = function signOff() {
	this.show(this.authForm);
	this.hide(this.profileInfo);
	this.clearProfileInfo();
}

// Определение класса, выполняющего аутентификацию

function AuthHelper(authUrl) {
	this.xhr = new XMLHttpRequest();
	this.xhr.helper = this;
	this.authUrl = authUrl || 'http://netology-hj-ajax.herokuapp.com/homework/login_json';
}

AuthHelper.prototype.auth = function auth(user, password) {
	var body = 'email=' + encodeURIComponent(user) + '&password=' + encodeURIComponent(password);
	this.xhr.open('POST', this.authUrl);
	this.xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	this.xhr.onloadstart = function () {
		this.helper.doOnAuthBegin();
	}
	this.xhr.onload = function () {
		if (this.status == 200) {
			var data = JSON.parse(this.responseText);
			if (data.error) {
				this.helper.doOnAuthFailed(data.error.message);
			} else {
				this.helper.doOnAuthSucceed(data);
			}
		} else {
			this.helper.doOnAuthFailed(this.status + ": " + this.statusText);
		}
	}
	this.xhr.onerror = function () {
		this.helper.doOnAuthFailed(this.status + ": " + this.statusText);
	}

	this.xhr.send(body);
}

AuthHelper.prototype.doOnAuthBegin = function () {
	if (this.onauthbegin) {
		this.onauthbegin();
	}
}

AuthHelper.prototype.doOnAuthSucceed = function (profileData) {
	if (this.onauthsucceed) {
		this.onauthsucceed(profileData);
	}
}

AuthHelper.prototype.doOnAuthFailed = function (message) {
	if (this.onauthfailed) {
		this.onauthfailed(message);
	}
}

var builder = new AuthViewBuilder();
